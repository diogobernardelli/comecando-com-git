# CONTROLE DE VERSÕES COM GIT

> By Diogo Bernardelli

## Baixando o projeto

```bash
$ git clone git@gitlab.com:diogobernardelli/comecando-com-git.git
```


## Guia de comandos

```bash
# help geral
$ git help

# instancia o git no diretório atual
$ git init

# inicia o 'git flow' no projeto (para facilitar a criação e gerenciamento de branches e features)
$ git init flow

# adiciona todos os arquivos alterados no projeto
$ git add .

# interface para controle de adição de arquivos
$ git add -i

# verifica modificações no projeto (adicionados / alterados / deletados)
$ git status

# faz commit de arquivos (após git add)
$ git commit -m 'Mensagem do commit'

# faz commit de arquivos que já estejam na 'head' (não precisa de git add)
$ git commit -am 'Mensagem do commit'

# histórico
$ git log

# atualizar os arquivos no branch atual
$ git pull

# enviar os arquivos alterados na branch atual
$ git push

# cancela a ação do 'git add'
$ git reset

# cancela a ação do 'git commit'
$ git reset HEAD~

# cancela as alterações em todos os arquivos
$ git checkout .

# troca de feature/branch
$ git checkout master

# deleta o arquivo (o -f força a exclusão caso o mesmo tenha sido adicionado ao HEAD)
$ git rm -f teste.html

# cria uma pilha com as alterações realizadas
$ git stash

# aplica as alterações o último git stash realizado
$ git stash apply

# lista toda a pilha de stashs
$ git stash list

# aplica um stash de id específico
$ git stash apply stash@{2}

# compara as alterações realizadas com a última versão
$ git diff teste.html

# verifica versão do commit e quem realizou as alterações em determinado arquivo
$ git blame teste.html

# faz a 'fusão' de uma feature/branch com outra feature/branch
$ git merge master

# ver todas as configurações
$ git config --list

# habilita autocorreção de comandos (3 segundos no exemplo abaixo)
$ git config --global help.autocorrect 30

# muda a cor de arquivos alterados ao realizar o comando 'git status'
$ git config --global color.status.changed "magenta white bold"
```

## Bônus: Bisect

O bisect (pesquisa binária) é útil para encontrar um commit que esta gerando um bug ou uma inconsistência entre uma sequência de commits.

##### Iniciar pequinsa binária

```bash
$ git bisect start
```

##### Marcar o commit atual como ruim

Se o commit estiver com o problema, então ele deverá ser marcado como **ruim**.

```bash
$ git bisect bad
```

##### Marcar o commit como bom

O GIT irá navegar entre os commits para ajudar a indentificar o commit que esta com o problema. Se o commit atual não estiver quebrado, então é necessário marca-lo como **bom**.

```bash
$ git bisect good
```

##### Finalizar a pesquisa binária

Depois de encontrar o commit com problema, para retornar para o *HEAD* utilize:

```bash
$ git bisect reset
```